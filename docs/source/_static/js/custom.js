$(document).ready(function(){
	try{
	$('.rst-content').find(".wy-breadcrumbs").find("a").each(function(){
		if($(this).text()=='Docs')
		{
			$(this).addClass("icon icon-home");
			$(this).text("");
		}
	});

	//display hr line after heading which is replaced in xml with conf.pf code block
    var isToolguid=false;
	$( "ul[class='wy-breadcrumbs']" ).find("li").each(function(){
		//console.log('li text :: '+$(this).text());
		//console.log('is contains Tool Guide :: '+($(this).text().indexOf('Tool Guide')));
		if($(this).text().indexOf('Tool Guide')>-1 || $(this).text().indexOf('ModusToolbox')>-1 || $(this).text().indexOf('Running')>-1)
			isToolguid = true;
	});
	
	//console.log('isToolguid :: '+isToolguid);
	if(!isToolguid)
	{
		$(".section p").each(function(){
			var paraText = $(this).text().trim();
			var isOnlyHeading=true;
			$(this).children().each(function(){
				var tagName = $(this).prop("tagName");
				
				if(tagName.toLowerCase()=='em' && $(this).text().trim() == paraText && $(this).text().trim()=="Values:")
				{
					$(this).after('<hr/>');
					$(this).css("display", "none");
				}
				else if (tagName.toLowerCase()=='em' && $(this).text().trim() == paraText)
				{
					$(this).after('<hr/>');
					$(this).addClass("headingclass");
				}
				else if (tagName.toLowerCase()=='img' )
					$(this).before("<br>")
			});
			
		});
		
		if($( ".section li > p > em" ).next().is( "hr" ))
		{
			$( ".section li > p > em" ).next().is( "hr" ).remove();
			$( ".section li > p > em" ).removeClass( "headingclass" );
		}
	}
	
	$('.highlight-default .highlight pre').each(function (e) {
        var spans = Array.prototype.slice.call(this.getElementsByTagName('span'));
        var startCmts = [], endCmts = [], startMethod = [];
        $.each(spans, function (index, value) {
            if ($(value).html() === '/*') {
                startCmts.push(index);
            }
            if ($(value).html() === '*/') {
                endCmts.push(index + 1);
            }
            if ($(value).html() === '(') {
                startMethod.push(index - 1);
            }
        });
        $.each(startCmts, function (index, value) {
            var sliced = spans.slice(startCmts[index], endCmts[index]);
            $.each(sliced, function (i, v) {
                v.removeAttribute("class");
                v.classList.add("comment");
            });
        });
        $.each(startMethod, function (index, value) {
            spans[value].removeAttribute("class");
            spans[value].classList.add("variableclass");
        });
    });
	}catch(err){console.log('err :: '+err);}
	
	//This is used to auto enable the toctree after 4 depth menu elements
	setTimeout(toctreeSettingFunction, 50);

});

function toctreeSettingFunction()
{
	$("a.current").parents("li[class^='toctree-']").each(function(){
		if(!($(this).hasClass("current" )))
		{
			$(this).addClass("current");
		}
	});
}


