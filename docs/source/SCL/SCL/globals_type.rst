=================
Typedefs
=================

-  scl_buffer_t : `scl_common.h <scl__common_8h.html#ade53c1ba949b59704f58c0c2d11d5421>`__
-  scl_event_handler_t : `scl_wifi_api.h <group__wifi.html#ga69d33b94e3ad497f7b8c1031bbe427c4>`__
-  scl_result_t : `scl_common.h <scl__common_8h.html#aa58a3303ad0c17b7a2d32b4b3f390ec4>`__
-  scl_scan_result_callback_t : `scl_wifi_api.h <group__wifi.html#gaf28de372f19220528a8020d489283291>`__
-  scl_wl_chanspec_t : `scl_types.h <scl__types_8h.html#ab797194a7b2cf547bec44324fbb117a4>`__

