======================
SCL communication API
======================

.. doxygengroup:: communication
   :project: SCL 
   :members:
   :protected-members:
   :private-members:
   :undoc-members: