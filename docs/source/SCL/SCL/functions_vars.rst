==========
Variables
==========

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  activity_cb :
         SCL_EMAC 
      -  assoc :
         `scl_listen_interval_t <structscl__listen__interval__t.html#af001610277052191979db17c0133c933>`__
      -  atim_window :
         wl_bss_info_struct 

      .. rubric:: - b -
         :name: b--

      -  band :
         scl_scan_result 
      -  basic_mcs :
         wl_bss_info_struct 
      -  beacon :
         `scl_listen_interval_t <structscl__listen__interval__t.html#a59de6cdff8214507260f142834f20cee>`__
      -  beacon_period :
         wl_bss_info_struct 
      -  bss_type :
         scl_scan_result 
      -  BSSID :
         scl_scan_result 
         ,
         scl_simple_scan_result 
         ,
         wl_bss_info_struct 
      -  buffer :
         `scl_tx_buf <group__wifi.html#ga6164f96b054c947752bce394af84b548>`__

      .. rubric:: - c -
         :name: c--

      -  capability :
         wl_bss_info_struct 
      -  ccode :
         scl_scan_result 
      -  channel :
         scl_scan_result 
         ,
         scl_simple_scan_result 
      -  chanspec :
         wl_bss_info_struct 
      -  connection_status :
         `network_params <group__communication.html#ga5ab54995772ff96d303a45287001664f>`__
      -  count :
         wl_bss_info_struct 
      -  ctl_ch :
         wl_bss_info_struct 

      .. rubric:: - d -
         :name: d--

      -  dtim :
         `scl_listen_interval_t <structscl__listen__interval__t.html#a011e406e7bba2200194b90d308ea2e82>`__
      -  dtim_period :
         wl_bss_info_struct 

      .. rubric:: - e -
         :name: e--

      -  emac_link_input_cb :
         SCL_EMAC 
      -  emac_link_state_cb :
         SCL_EMAC 

      .. rubric:: - f -
         :name: f--

      -  flags :
         scl_scan_result 
         ,
         wl_bss_info_struct 

      .. rubric:: - g -
         :name: g--

      -  gateway :
         `network_params <group__communication.html#ga3cd34dd09ff5b61fd63a489a7cf8ae7c>`__

      .. rubric:: - i -
         :name: i--

      -  ie_len :
         scl_scan_result 
      -  ie_length :
         wl_bss_info_struct
      -  ie_offset :
         wl_bss_info_struct 
      -  ie_ptr :
         scl_scan_result 
      -  interface_type :
         SCL_EMAC 
      -  ip_address :
         `network_params <group__communication.html#gac5432aed416c5c95736350828bb7b64d>`__

      .. rubric:: - l -
         :name: l--

      -  length :
         `scl_ssid_t <structscl__ssid__t.html#ab2b3adeb2a67e656ff030b56727fd0ac>`__
         ,
         wl_bss_info_struct 
      -  link_state :
         SCL_EMAC 

      .. rubric:: - m -
         :name: m--

      -  max_data_rate :
         scl_scan_result 
      -  memory_manager :
         SCL_EMAC 
      -  multicast_addr :
         SCL_EMAC 

      .. rubric:: - n -
         :name: n--

      -  n_cap :
         wl_bss_info_struct
      -  nbss_cap :
         wl_bss_info_struct 
      -  netmask :
         `network_params <group__communication.html#gabe8b36c35606ee162f7e876cd4d93b7d>`__
      -  next :
         scl_scan_result 
      -  number_of_probes_per_channel :
         scl_scan_extended_params_t 

      .. rubric:: - o -
         :name: o--

      -  octet :
         `scl_mac_t <structscl__mac__t.html#abc3755f1f66dea95fce153ee4f49e907>`__

      .. rubric:: - p -
         :name: p--

      -  phy_noise :
         wl_bss_info_struct 
      -  powered_up :
         SCL_EMAC 
		 
      .. rubric:: - r -
         :name: r--

      -  rates :
         wl_bss_info_struct 
      -  rateset :
         wl_bss_info_struct 
      -  reserved :
         wl_bss_info_struct 
      -  reserved32 :
         wl_bss_info_struct 
      -  RSSI :
         wl_bss_info_struct 

      .. rubric:: - s -
         :name: s--

      -  scan_active_dwell_time_per_channel_ms :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#addbd186dd08b75e5de298a3d8dbb76a1>`__
      -  scan_home_channel_dwell_time_between_channels_ms :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#a325bae58ff955b428047edab0a2f0799>`__
      -  scan_passive_dwell_time_per_channel_ms :
         `scl_scan_extended_params_t <structscl__scan__extended__params__t.html#afe48c256e4f4f14eb202d51dd8348818>`__
      -  security :
         scl_scan_result 
         ,
         scl_simple_scan_result 
      -  signal_strength :
         scl_scan_result 
         ,
         scl_simple_scan_result 
      -  size :
         `scl_tx_buf <group__wifi.html#gab2c6b258f02add8fdf4cfc7c371dd772>`__
      -  SNR :
         wl_bss_info_struct 
      -  SSID :
         scl_scan_result 
         ,
         scl_simple_scan_result 
         ,
         wl_bss_info_struct 
      -  SSID_len :
         wl_bss_info_struct 

      .. rubric:: - v -
         :name: v--

      -  value :
         `scl_ssid_t <structscl__ssid__t.html#aa88a4115b417ed84082f85ab347f4b02>`__
      -  version :
         wl_bss_info_struct 

.. container:: navpath
   :name: nav-path

   -  Copyright Cypress Corporation.

.. |Cypress Logo| image:: cypress_logo.png
