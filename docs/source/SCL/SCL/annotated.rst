================
Data Structures
================


Here are the data structures with brief descriptions:

+----------------------------------+----------------------------------+
| `network_params_t <s             | Network parameters structure     |
| tructnetwork__params__t.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `scl_event_msg                   | Structure to store fields after  |
| <structscl__event__msg.html>`__  | ethernet header in event message |
+----------------------------------+----------------------------------+
|                                  | Structure for storing 802.11     |
| `scl_listen_interval_t <structs  | powersave listen interval values |
| cl__listen__interval__t.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `scl_m                           | Structure for storing a MAC      |
| ac_t <structscl__mac__t.html>`__ | address (Wi-Fi Media Access      |
|                                  | Control address)                 |
+----------------------------------+----------------------------------+
| `scl_scan_                       | Structure for storing extended   |
| extended_params_t <structscl__sc | scan parameters                  |
| an__extended__params__t.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `scl_scan_result_t <str          | Structure for storing scan       |
| uctscl__scan__result__t.html>`__ | results                          |
+----------------------------------+----------------------------------+
| `scl_ssi                         | Structure for storing a Service  |
| d_t <structscl__ssid__t.html>`__ | Set Identifier (i.e              |
+----------------------------------+----------------------------------+
| `s                               | Structure to store scan result   |
| cl_sync_scan_result_t <structscl | parameters for each AP           |
| __sync__scan__result__t.html>`__ |                                  |
+----------------------------------+----------------------------------+
| `scl_tx_buf_t                    | SCL transmit buffer structure    |
| <structscl__tx__buf__t.html>`__  |                                  |
+----------------------------------+----------------------------------+
| `scl_wl_bss_info_t <stru         | BSS(Basic Service Set)           |
| ctscl__wl__bss__info__t.html>`__ | information structure            |
+----------------------------------+----------------------------------+

  

   class_scl_access_point.rst
   class_scl_s_t_a_interface.rst
   structwl__bss__info__struct.rst
   

.. toctree::
   :hidden:
   
   structnetwork__params__t.rst
   structscl__event__msg.rst
   structscl__listen__interval__t.rst
   structscl__mac__t.rst
   structscl__scan__extended__params__t.rst
   structscl__scan__result__t.rst
   structscl__ssid__t.rst
   structscl__sync__scan__result__t.rst
   structscl__tx__buf__t.rst
   structscl__wl__bss__info__t.rst
