===========
Enumerator
===========

s
==

-  SCL_802_11_BAND_2_4GHZ :
   `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64da6d111a8b6f67923956f9129e9872cf0f>`__
-  SCL_802_11_BAND_5GHZ :
   `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64dae5d47ffbe5bfd0a406b4f44d9b6a9c6c>`__
-  SCL_AP_ROLE :
   `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a676bde82f54e54e5292eb421cb3e276d>`__
-  SCL_BSS_TYPE_ADHOC :
   `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fab969be0fc08cae910fcfb820d7663ed6>`__
-  SCL_BSS_TYPE_ANY :
   `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa4a800e68a8f050f9faff956ce591b82a>`__
-  SCL_BSS_TYPE_INFRASTRUCTURE :
   `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa7b039658e58a158ce54a1219e026fc91>`__
-  SCL_BSS_TYPE_MESH :
   `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa643a49227a45f193bdabbef53f114488>`__
-  SCL_BSS_TYPE_UNKNOWN :
   `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92fa13a1230cb01063bb97731ce9674bc775>`__
-  SCL_FALSE :
   `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812ae2b5f551535df41fa57331942b7050d0>`__
-  SCL_INVALID_ROLE :
   `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a9a76612f324aa731465b8fcef1085533>`__
-  SCL_NETWORK_RX :
   `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0a429fd371188f3dd267d8b7f078347479>`__
-  SCL_NETWORK_TX :
   `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0abf8bfee8fd4b7ba0441cf63af353c628>`__
-  SCL_NSAPI_SECURITY_CHAP :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3acef574147829bf2567cec2f9f4564bf0>`__
-  SCL_NSAPI_SECURITY_EAP_TLS :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a19bca4c0656a4b89382fc265218e6228>`__
-  SCL_NSAPI_SECURITY_NONE :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a9eac5dc17b5a2002167ec6d315f46bc8>`__
-  SCL_NSAPI_SECURITY_PAP :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a842bf987118141ea20f5250733bf1708>`__
-  SCL_NSAPI_SECURITY_PEAP :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a6df810e80cece34d26b8b2d717420dc1>`__
-  SCL_NSAPI_SECURITY_UNKNOWN :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3aca972659886e5b8a901041f5154fcc93>`__
-  SCL_NSAPI_SECURITY_WEP :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a2dcd7dd88cc28fffb75f39d507dc7422>`__
-  SCL_NSAPI_SECURITY_WPA :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3aefe0793e05e344bd7a1800af56495b94>`__
-  SCL_NSAPI_SECURITY_WPA2 :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a3ce72390c748d46213d2f955c3d1d388>`__
-  SCL_NSAPI_SECURITY_WPA2_ENT :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3ab35cac5ce315530ba060c28b2fb7e44b>`__
-  SCL_NSAPI_SECURITY_WPA_WPA2 :
   `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3a5877b5e0ff37391a92c4db37eb00cbe3>`__
-  SCL_NSAPI_STATUS_CONNECTING :
   `scl_common.h <scl__common_8h.html#aafcfea88c2d16fddafb15745a6ecaba8a7cecd016ce40d4ee2687374bc168ed5e>`__
-  SCL_NSAPI_STATUS_DISCONNECTED :
   `scl_common.h <scl__common_8h.html#aafcfea88c2d16fddafb15745a6ecaba8a1a88a3920e882f424f2efc4c803ff009>`__
-  SCL_NSAPI_STATUS_GLOBAL_UP :
   `scl_common.h <scl__common_8h.html#aafcfea88c2d16fddafb15745a6ecaba8a7da44a711759e8ac993eee1fddf0314f>`__
-  SCL_NSAPI_STATUS_LOCAL_UP :
   `scl_common.h <scl__common_8h.html#aafcfea88c2d16fddafb15745a6ecaba8afeea1368946d765d9881fdafb755a5e2>`__
-  SCL_P2P_ROLE :
   `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a586d733a3fb6470ef16a56dd4942da40>`__
-  SCL_RX_DATA :
   `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a1ef23948fa9e7e7fac502b8eb189ea47>`__
-  SCL_RX_EVENT_CALLBACK :
   `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8abd1b1ece53a2a2c6a85851ec16fd510b>`__
-  SCL_RX_GET_BUFFER :
   `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a814357835155c8c6edfb25056e992351>`__
-  SCL_RX_GET_CONNECTION_STATUS :
   `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a284dec35e181230494fdbb3e8db87074>`__
-  SCL_RX_SCAN_STATUS :
   `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8a63bfd294e077ab2ac1c9f67af47f0825>`__
-  SCL_RX_TEST_MSG :
   `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8ad41fc8f197fa39f802e1626e8a9990e2>`__
-  SCL_SCAN_ABORTED :
   `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1bad4f3132ab91a0e896e7477b92dbd5075>`__
-  SCL_SCAN_COMPLETED_SUCCESSFULLY :
   `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1bad39efc1c23ec7a0c7d67f568e6f52a85>`__
-  SCL_SCAN_INCOMPLETE :
   `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1ba9d48c63c00486124e6bc25d8db1ec138>`__
-  SCL_SCAN_TYPE_ACTIVE :
   `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea43c77a264c5a68ba7402d272d5818485>`__
-  SCL_SCAN_TYPE_NO_BSSID_FILTER :
   `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea522c7c1ae2ff3acd70bc336ec14bc50e>`__
-  SCL_SCAN_TYPE_PASSIVE :
   `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea325ca62baea89d873496847dc2f6e086>`__
-  SCL_SCAN_TYPE_PNO :
   `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47eadbab2440a09df91733882418c643c607>`__
-  SCL_SCAN_TYPE_PROHIBITED_CHANNELS :
   `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47ea9e84ff288b7e8c32359d5d6c1898804d>`__
-  SCL_SECURITY_FORCE_32_BIT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a35af276654937141437e8c912966ac73>`__
-  SCL_SECURITY_IBSS_OPEN :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ad886638286e66e06c8e3fa714146c9a0>`__
-  SCL_SECURITY_OPEN :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a86f904c2b846eef773b9200212046dcd>`__
-  SCL_SECURITY_UNKNOWN :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a8bda7dcf292a27fe457096fbabeb59eb>`__
-  SCL_SECURITY_WEP_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ad48007b9e1fe1972d5c4070642d7f26b>`__
-  SCL_SECURITY_WEP_SHARED :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ab7c67fff083e10fe48d11c29558b1d39>`__
-  SCL_SECURITY_WPA2_AES_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67af5c5e8ba6b8c3de871d3c989b2691a4e>`__
-  SCL_SECURITY_WPA2_AES_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa88617f6384eea8a0f7a6abe0f84ec81>`__
-  SCL_SECURITY_WPA2_FBT_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a0e83dca81b6fe104e6f9cf45a07bac80>`__
-  SCL_SECURITY_WPA2_FBT_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a5663ce3870a19a2d01c816e54417f2ba>`__
-  SCL_SECURITY_WPA2_MIXED_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa23b01657d269dadc56692a9655ca69b>`__
-  SCL_SECURITY_WPA2_MIXED_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae28655d240b846e6cfd00676704d322d>`__
-  SCL_SECURITY_WPA2_TKIP_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67adccfe4e43b721376adeb3a5bac94a714>`__
-  SCL_SECURITY_WPA2_TKIP_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae1efdd5c152b318166d6f6560393261e>`__
-  SCL_SECURITY_WPA2_WPA_AES_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a6846340f2b57b8e1406cb5952d4d1aec>`__
-  SCL_SECURITY_WPA2_WPA_MIXED_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a2c145ac8272198c2dd3ad9949674a4c6>`__
-  SCL_SECURITY_WPA2_WPA_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ac9cb906c3b88a9bdc0ade00651f0c771>`__
-  SCL_SECURITY_WPA2_WPA_TKIP_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a170cf7999b9e915c45d945e72e6ef087>`__
-  SCL_SECURITY_WPA3_SAE :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a23e95b5998fa405fecd423e416cff179>`__
-  SCL_SECURITY_WPA3_WPA2_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a5d9d2fe7c0130877be4e2f8d1d7f2311>`__
-  SCL_SECURITY_WPA_AES_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67acee17758cf43103a3a3b8736f7ffa032>`__
-  SCL_SECURITY_WPA_AES_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a29a27e98cf3c95da855b5577825d270e>`__
-  SCL_SECURITY_WPA_MIXED_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67aa8e4e60ef3977f23abae498e391dcb81>`__
-  SCL_SECURITY_WPA_MIXED_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a328af64a5bfe6959a75d150bb500f4fe>`__
-  SCL_SECURITY_WPA_TKIP_ENT :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a644c609edc87ebba035f2de49ac62f1a>`__
-  SCL_SECURITY_WPA_TKIP_PSK :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ae98e5d51446406bd53805807f40c70b8>`__
-  SCL_SECURITY_WPS_OPEN :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67a42bca33cf42db8341589161fc90a08ac>`__
-  SCL_SECURITY_WPS_SECURE :
   `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67ac25ebb997080ba93473a3171bb168393>`__
-  SCL_STA_ROLE :
   `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263a8f8f3eacf8c09454172a590657d78afc>`__
-  SCL_TRUE :
   `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812a25c4ff8d5a12ed99b5d6f0dc175ea47d>`__
-  SCL_TX_CONFIG_PARAMETERS :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46affbde7bce68d8585e5dc6f811e534aec>`__
-  SCL_TX_CONNECT :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a234e28ed2e4ef5b47b7dec4315b87dc5>`__
-  SCL_TX_CONNECTION_STATUS :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a1833ff3bb744ded966d7e303f661d584>`__
-  SCL_TX_DISCONNECT :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a404a155243899240e4066042d05f40bb>`__
-  SCL_TX_GET_BSS_INFO :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a0649cf7412376be99526a5cb9aee8460>`__
-  SCL_TX_GET_MAC :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46aa83a436dbdb4c3f2a0f30cb007db706a>`__
-  SCL_TX_REGISTER_MULTICAST_ADDRESS :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46af78db32b364393f00bdfa8532851ffd0>`__
-  SCL_TX_SCAN :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a819078a6ac44ea3c25516668db264967>`__
-  SCL_TX_SCL_VERSION_NUMBER :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab469ff0c31036aedc93e0e19f3acc749>`__
-  SCL_TX_SEND_OUT :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46affc21552e23ab5fb0f3467063a01cc02>`__
-  SCL_TX_SET_EVENT_HANDLER :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab78424ac3b707a394fafb78b94356001>`__
-  SCL_TX_SET_IOCTL_VALUE :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a6352d889000eb6508f11d4bd7cf464d6>`__
-  SCL_TX_TEST_MSG :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a58efee8e7e8769d8685fc1b6567ed911>`__
-  SCL_TX_TRANSCEIVE_READY :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a8b0a03e5769e80b5ac4bfece90968f7c>`__
-  SCL_TX_WIFI_GET_BSSID :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a9cb54f84da32c6a983adc1f96890ce4b>`__
-  SCL_TX_WIFI_GET_RSSI :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ac52dd1ad608863e2561bd91c2cc76dfc>`__
-  SCL_TX_WIFI_INIT :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a8578fe2e7e153cbb90238ddab4578472>`__
-  SCL_TX_WIFI_JOIN :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a12f3edbd6559174029d270339a125c33>`__
-  SCL_TX_WIFI_NW_PARAM :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46aeba990e60a370115fddfab01fb80efbc>`__
-  SCL_TX_WIFI_ON :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46ab59880910176cc776f5b44520068bb9c>`__
-  SCL_TX_WIFI_SET_UP :
   `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46a88fe5a393e75e5c10f9f18eaeba1cd5a>`__
-  SCL_WLC_E_ACTION_FRAME_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a560e46252edd14605cbd9936a18b5b36>`__
-  SCL_WLC_E_ACTION_FRAME_OFF_CHAN_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6e6552c59403f7aefc5ed150180a2d46>`__
-  SCL_WLC_E_ACTION_FRAME_RX :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6f8a08c60d1fd9fc49d13db377b68622>`__
-  SCL_WLC_E_ACTION_FRAME_RX_NDIS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a58f9de06400789f74ba59cdc50bdf5da>`__
-  SCL_WLC_E_ADDTS_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a03c7dbe1f13ab0b2a5ef7b59b3f06a3b>`__
-  SCL_WLC_E_AP_STARTED :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a54308864c1e11d221e3492d0c0a91bf9>`__
-  SCL_WLC_E_ASSOC :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a0d4e06953a012521548a2043f3952822>`__
-  SCL_WLC_E_ASSOC_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a3749fca75928318bc137bd3d13bee1b1>`__
-  SCL_WLC_E_ASSOC_REQ_IE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aa1e1eaaa5f9c62be18322b89fccdb482>`__
-  SCL_WLC_E_AUTH :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aef6e7ee23ee2d1102e258b3efc8b4911>`__
-  SCL_WLC_E_AUTH_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a417311498fb730320c05e28a1cef934e>`__
-  SCL_WLC_E_AUTH_REQ :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a134a85034cd2deb189df6fc5115b6dfb>`__
-  SCL_WLC_E_AUTOAUTH :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ab7b8c85ab7152fffd3bd2f596c1801f4>`__
-  SCL_WLC_E_AWDL_AW :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a8ba3ecac77478c8b582bc2cdc3a01ae4>`__
-  SCL_WLC_E_AWDL_AW_END :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a16623140f7f079fcdbc837eb96568a50>`__
-  SCL_WLC_E_AWDL_AW_EXT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aec62e3a1c548cefa0970d4197e65dab9>`__
-  SCL_WLC_E_AWDL_AW_START :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a930646bd7c85cae1b237942976c7ba6c>`__
-  SCL_WLC_E_AWDL_EVENT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504abdced9bd9664be70d875656cc8a4c8b8>`__
-  SCL_WLC_E_AWDL_OOB_AF_STATUS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aea1487cc6807e4a3425242bd048dcbf4>`__
-  SCL_WLC_E_AWDL_PEER_CACHE_CONTROL :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a9ed09727d414ae072261e9c40b2e0f2a>`__
-  SCL_WLC_E_AWDL_PHYCAL_STATUS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a34e0ae01a42f0e05e91d922ab0e506dc>`__
-  SCL_WLC_E_AWDL_ROLE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a49bd9f9ffae403a681efd075c939c704>`__
-  SCL_WLC_E_AWDL_RX_ACT_FRAME :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a3f291ca3122754a53bd39bbbb637f500>`__
-  SCL_WLC_E_AWDL_RX_PRB_RESP :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aa84ef24a956d3dd97251d1ae2df20e75>`__
-  SCL_WLC_E_AWDL_SCAN_STATUS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a0fbe1b06fe060602db5de4bc7a3a7bb0>`__
-  SCL_WLC_E_AWDL_WOWL_NULLPKT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ac937213d2be6432e76b39e83493694f9>`__
-  SCL_WLC_E_BCMC_CREDIT_SUPPORT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504adb379db22c3914b7d6478d4b37a53a0e>`__
-  SCL_WLC_E_BCNLOST_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6ad36cb4f1feee3a99e5a9cbf4e2db83>`__
-  SCL_WLC_E_BCNRX_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ad9a5d2e122434d2922003816e89f1f42>`__
-  SCL_WLC_E_BCNSENT_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6a4d5833cd73ae61ed1eda393dae3d66>`__
-  SCL_WLC_E_BEACON_FRAME_RX :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a7bed2f5f4ce866050db7c065e7058707>`__
-  SCL_WLC_E_BEACON_RX :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a5d13f5b213f3a9e25b63a6f08e108e04>`__
-  SCL_WLC_E_BSSID :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ad72be492658b1392352cbd1a8618dbf7>`__
-  SCL_WLC_E_BT_WIFI_HANDOVER_REQ :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504af7a9ef5c25789133757b1d3ea60d3324>`__
-  SCL_WLC_E_CCX_ASSOC_ABORT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504af30ec397415123b4f98bbee20cd9d8b2>`__
-  SCL_WLC_E_CCX_ASSOC_START :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a33598eb420a1a2a2b50555564c13eef3>`__
-  SCL_WLC_E_CHANNEL_ADOPTED :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6db4947a49723ce14f09d44170bee10e>`__
-  SCL_WLC_E_COUNTRY_CODE_CHANGED :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a7f7915456a8330460377f91c2c2f91ee>`__
-  SCL_WLC_E_CSA_COMPLETE_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a62ff260ace8b87796299c180329d6046>`__
-  SCL_WLC_E_DCS_REQUEST :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ac55e66bc3156c0f8af512b232e190e72>`__
-  SCL_WLC_E_DEAUTH :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ae8cd2d04932d17b7a7e85aca950d8a8d>`__
-  SCL_WLC_E_DEAUTH_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a4e5c6d2919e4ff43550524c7670d8c1c>`__
-  SCL_WLC_E_DELTS_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a433faaa62b8ad0110794c843ea278967>`__
-  SCL_WLC_E_DFS_AP_RESUME :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a67836b893934f764e4ecc4a96b99034f>`__
-  SCL_WLC_E_DFS_AP_STOP :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504af21c2f5d0711ae83f595a7e1bb90bd83>`__
-  SCL_WLC_E_DISASSOC :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a8b8ce2cd9bead148fdcd19a63a358f83>`__
-  SCL_WLC_E_DISASSOC_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a684e8c6a2f8ca3f94f4750c048dacb5e>`__
-  SCL_WLC_E_EAPOL_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a26bbe02c4460f841333e0f90efee2d3e>`__
-  SCL_WLC_E_ESCAN_RESULT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a1b39685f4e60c0b2ddacb57c1e164362>`__
-  SCL_WLC_E_EXTLOG_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504adaaaaedcd75dd14c0cb9a8dbb8ad9ffe>`__
-  SCL_WLC_E_GAS_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a2e2bbd7ab502bd446c865b72d5df4b59>`__
-  SCL_WLC_E_GAS_FRAGMENT_RX :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6ce325b4e0fac3c3658275c909ec66df>`__
-  SCL_WLC_E_GTK_PLUMBED :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a27fee71167b1ab417e1019cb51edb930>`__
-  SCL_WLC_E_HTSFSYNC :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a4bab12cfddf44ac3ca90cf333b49fa64>`__
-  SCL_WLC_E_IBSS_COALESCE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afe10f2db8bc973217665b783a49ffa74>`__
-  SCL_WLC_E_ICV_ERROR :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ab238cc4fe575ac1c4d6241c8d15fc417>`__
-  SCL_WLC_E_IF :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a3935011c1935cf4add2e7a1a4ed3a867>`__
-  SCL_WLC_E_LINK :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afe5c4901c57f49aee9b55ac1baa5addd>`__
-  SCL_WLC_E_MESH_DHCP_SUCCESS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afbc86bcb5a565d819f696f195c79b488>`__
-  SCL_WLC_E_MESH_PAIRED :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a81069a7eca971d1d3cb82cb885e068e6>`__
-  SCL_WLC_E_MIC_ERROR :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a73d69363a67b26826ed243ba6d1b7af7>`__
-  SCL_WLC_E_MULTICAST_DECODE_ERROR :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a96a2636b51bccb6184e6d6cd5acd607c>`__
-  SCL_WLC_E_NAN :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a1880f11d58f0473364635c5b94b77f4d>`__
-  SCL_WLC_E_NATIVE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a313c2e4f536cfbb845adb6792b76c79a>`__
-  SCL_WLC_E_NDIS_LINK :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a144efea5d99370203773fa66aa6ce987>`__
-  SCL_WLC_E_NIC_AF_TXS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a5225316e5367cc5f078985e2a343e58b>`__
-  SCL_WLC_E_OVERLAY_REQ :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a7384cd87ce947a06a1f6ec019e1af19b>`__
-  SCL_WLC_E_P2P_DISC_LISTEN_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504abaa8579e22e2558a91bc9858414d3261>`__
-  SCL_WLC_E_P2P_PROBREQ_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a23ecab2a7de6667a72b2836297a58f9f>`__
-  SCL_WLC_E_P2PO_ADD_DEVICE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a7445c9c7f6a6337cd4ecf9e57c6c0329>`__
-  SCL_WLC_E_P2PO_DEL_DEVICE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a2744eaf5e4968b9ebc35cff06e754d86>`__
-  SCL_WLC_E_PFN_BEST_BATCHING :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a64f67aa0510f0a4d0420695cb118fe9a>`__
-  SCL_WLC_E_PFN_NET_FOUND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a1820d1e21347758e990d28fc91076185>`__
-  SCL_WLC_E_PFN_NET_LOST :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a0a2417db7affd86c42ab586288fd4a4f>`__
-  SCL_WLC_E_PFN_SCAN_ALLGONE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a162e32a0101bbfcc3534ee962f9dad13>`__
-  SCL_WLC_E_PFN_SCAN_NONE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afd77de06bf2a39f398fc91f9c0ff549f>`__
-  SCL_WLC_E_PKTDELAY_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ab40a2f178dfab3d0b29bf841abe1567c>`__
-  SCL_WLC_E_PMKID_CACHE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a97266d0748f152f580e3469433927f52>`__
-  SCL_WLC_E_PRE_ASSOC_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a542640316cd0a91218022d0a0ef7ca4d>`__
-  SCL_WLC_E_PRE_REASSOC_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ae751b22adf2b638a6fadbecc948120de>`__
-  SCL_WLC_E_PROBREQ_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504af9bd2ee06634fd0f92ee9c966d222b2f>`__
-  SCL_WLC_E_PROBRESP_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aecc7717ce114a45bbfa761385368ac29>`__
-  SCL_WLC_E_PROXD :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a9ecf30c5f2e6674589b968e0d430317b>`__
-  SCL_WLC_E_PRUNE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504aa06e0c5a453aaf4c2ba0902529302fda>`__
-  SCL_WLC_E_PSTA_PRIMARY_INTF_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ac7c831904841156c47e3e6c581d22648>`__
-  SCL_WLC_E_QUIET_END :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afd9167ab090a85ac9b7d9fbea8a49739>`__
-  SCL_WLC_E_QUIET_START :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6a2850bdbf8be6db42306b3b64c949e9>`__
-  SCL_WLC_E_REASSOC :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a30ce84d8f8f0a946c3234b50f846dd16>`__
-  SCL_WLC_E_REASSOC_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ad6ca0c4ae62150d515d0fa85d469dbba>`__
-  SCL_WLC_E_REASSOC_IND_NDIS :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a8161f79be7bc3169dbe4b625283f385d>`__
-  SCL_WLC_E_RESET_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a7a6a3c16a7da2c98cce5e08a797ae1f4>`__
-  SCL_WLC_E_RETROGRADE_TSF :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afcd85e73b7daa4e0598503423b59287c>`__
-  SCL_WLC_E_RM_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a5ec8e41163fe8282a49ca4a99d6a9b3a>`__
-  SCL_WLC_E_ROAM :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a3a7f08a090eea87c341930dc6ba0b722>`__
-  SCL_WLC_E_ROAM_PREP :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a98c053877371849ea153963f954fdeb0>`__
-  SCL_WLC_E_RSSI :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ab2f33f8d06d69c9dd060e3b6cca1b9db>`__
-  SCL_WLC_E_SCAN_COMPLETE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504afe3f315623da56dc00dc5e6ea0ad61e5>`__
-  SCL_WLC_E_SCAN_CONFIRM_IND :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a34235bb46ce51426f26a4d292d02f457>`__
-  SCL_WLC_E_SPEEDY_RECREATE_FAIL :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a1e7ecd058df9a986c2b83139ef72835b>`__
-  SCL_WLC_E_START :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ace7e6d0ceee0c85052feb38506182840>`__
-  SCL_WLC_E_TDLS_PEER_EVENT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a83a0f7c3d62bf90d68ace4536e93e951>`__
-  SCL_WLC_E_TRACE :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ad243cbcd26ed47a28540ce7d9dd341ab>`__
-  SCL_WLC_E_TX_STAT_ERROR :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a5aba03179fb7661f862df1525d964d7a>`__
-  SCL_WLC_E_TXFAIL :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ac696332de7d07e5278f00a89cba0e1f1>`__
-  SCL_WLC_E_TXFAIL_THRESH :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a6725d24501e905cc8a8b25cea986998f>`__
-  SCL_WLC_E_UNICAST_DECODE_ERROR :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a3a14e667673d98d866f08df100b35693>`__
-  SCL_WLC_E_WAI_MSG :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a2ec2f525b5047b680ecf812d4ad6c328>`__
-  SCL_WLC_E_WAI_STA_EVENT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a4a2c6d925045e621e490181dd4e142e5>`__
-  SCL_WLC_E_WAKE_EVENT :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504ab167b49bf71ad3196f0ea8e6726e1e70>`__
-  SCL_WLC_E_WNM_STA_SLEEP :
   `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504a4281b8231a2dc5ad92181d8cdacdd0cd>`__
