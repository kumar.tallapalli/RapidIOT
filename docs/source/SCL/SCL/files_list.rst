==========
Files List
==========

Here is a list of all documented files with brief descriptions:

+------------------------------------------------------+--------------------------------------------------------------------+
| `scl_buffer_api.h <scl__buffer__api_8h.html>`__      | Provides declarations for buffer management functionality          |
+------------------------------------------------------+--------------------------------------------------------------------+
| `scl_common.h <scl__common_8h.html>`__               | Defines common data types used in SCL                              |
+------------------------------------------------------+--------------------------------------------------------------------+
| `scl_ipc.h <scl__ipc_8h.html>`__                     | Provides SCL functionality to communicate with Network Processor   |
+------------------------------------------------------+--------------------------------------------------------------------+
| `scl_types.h <scl__types_8h.html>`__                 | Defines common data types used in SCL                              |
+------------------------------------------------------+--------------------------------------------------------------------+
| `scl_wifi_api.h <scl__wifi__api_8h.html>`__          | Prototypes of functions for controlling the Wi-Fi system           |
+------------------------------------------------------+--------------------------------------------------------------------+


.. toctree::
   :maxdepth: 8
   :hidden:
   
   scl__buffer__api_8h.rst
   scl__common_8h.rst
   scl__ipc_8h.rst
   scl__types_8h.rst
   scl__wifi__api_8h.rst

