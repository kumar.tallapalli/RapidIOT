==
r
==

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented functions, variables, defines,
         enums, and typedefs with links to the documentation:

      .. rubric:: - r -
         :name: r--

      -  REFERENCE_DEBUG_ONLY_VARIABLE :
         `scl_types.h <scl__types_8h.html#a1491cb4c4adc44f22a91f18609dfb2f7>`__
      -  REG_IPC_STRUCT_DATA0 :
         `scl_ipc.h <scl__ipc_8h.html#a19afb8bdb807699c1ee4afc56a0ca078>`__


