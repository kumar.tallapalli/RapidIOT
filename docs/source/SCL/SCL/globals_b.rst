==
b
==

   .. container:: contents

      .. container:: textblock

         Here is a list of all documented functions, variables, defines,
         enums, and typedefs with links to the documentation:

      .. rubric:: - b -
         :name: b--

      -  BDC_HEADER_WITH_PAD :
         `scl_common.h <scl__common_8h.html#a50f830e25a5ac6d7d0e3eb8d23e90943>`__


