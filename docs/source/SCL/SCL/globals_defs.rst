=======
Macros
=======

.. contents::
   :depth: 3
..

.. container::
   :name: top

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  AES_ENABLED :
         `scl_types.h <scl__types_8h.html#a577a9dbc615ba145496a41a2035bd615>`__

      .. rubric:: - b -
         :name: b--

      -  BDC_HEADER_OFFSET_TO_DATA :
         `scl_common.h <scl__common_8h.html#a559655385d80ca97754fafa29ac87717>`__
      -  BDC_HEADER_WITH_PAD :
         `scl_common.h <scl__common_8h.html#a50f830e25a5ac6d7d0e3eb8d23e90943>`__
      -  BUFFER_OVERHEAD :
         `scl_common.h <scl__common_8h.html#a9b159bb86ca28f2200f30661441f232e>`__

      .. rubric:: - c -
         :name: c--

      -  CHECK_BUFFER_NULL :
         `scl_common.h <scl__common_8h.html#a28f8bd77cd7d514f350a9206bbd1cd28>`__

      .. rubric:: - e -
         :name: e--

      -  ENTERPRISE_ENABLED :
         `scl_types.h <scl__types_8h.html#ada6d6331053c0f88c63d77ba8d2019c8>`__

      .. rubric:: - f -
         :name: f--

      -  FBT_ENABLED :
         `scl_types.h <scl__types_8h.html#aafdddf85f4d0df08ee3aead815ad7c76>`__

      .. rubric:: - i -
         :name: i--

      -  IBSS_ENABLED :
         `scl_types.h <scl__types_8h.html#a9640c064932a3a633a3312b737658f83>`__
      -  INCLUDED_SCL_IPC_H :
         `scl_ipc.h <scl__ipc_8h.html#ac490449db752a75710f408a086919b0c>`__

      .. rubric:: - m -
         :name: m--

      -  MAX_BUS_HEADER_SIZE :
         `scl_common.h <scl__common_8h.html#aab3fad8b23f2db6d6c985134359bec0d>`__
      -  MCSSET_LEN :
         `scl_types.h <scl__types_8h.html#aa44a8901a98f9868efefee50db967736>`__
      -  MODULE_BASE_CODE :
         `scl_common.h <scl__common_8h.html#affac105f255c993640a9b157010b3dad>`__

      .. rubric:: - n -
         :name: n--

      -  NO_POWERSAVE_MODE :
         `scl_types.h <scl__types_8h.html#a287bbd4f521f0d1cf44165bc617cebf5>`__
      -  NW_CONNECT_TIMEOUT :
         `scl_ipc.h <scl__ipc_8h.html#a857645445c8f87898c14135ec9c07814>`__
      -  NW_DELAY_TIME_US :
         `scl_ipc.h <scl__ipc_8h.html#a335c0dd554fd890eb945a7daa99dd419>`__
      -  NW_DISCONNECT_TIMEOUT :
         `scl_ipc.h <scl__ipc_8h.html#afb7e03f043964993fdb3d15591d6e458>`__

      .. rubric:: - p -
         :name: p--

      -  PARAM_LEN :
         `scl_ipc.h <scl__ipc_8h.html#a9fec1f6a9b3f91e0171263391663f6ea>`__
      -  PM1_POWERSAVE_MODE :
         `scl_types.h <scl__types_8h.html#a32f56429462855603066fea3723c5217>`__
      -  PM2_POWERSAVE_MODE :
         `scl_types.h <scl__types_8h.html#af29e5543837b68c29417a7d15e3228b7>`__

      .. rubric:: - r -
         :name: r--

      -  REFERENCE_DEBUG_ONLY_VARIABLE :
         `scl_types.h <scl__types_8h.html#a1491cb4c4adc44f22a91f18609dfb2f7>`__
      -  REG_IPC_STRUCT_DATA0 :
         `scl_ipc.h <scl__ipc_8h.html#a19afb8bdb807699c1ee4afc56a0ca078>`__

      .. rubric:: - s -
         :name: s--

      -  SCL_ACCESS_POINT_NOT_FOUND :
         `scl_common.h <scl__common_8h.html#a4c4edaa8649ebd374238c479c1c6d787>`__
      -  SCL_ADDRESS_ALREADY_REGISTERED :
         `scl_common.h <scl__common_8h.html#a694c381b90996fe878150bb617904292>`__
      -  SCL_AP_ALREADY_UP :
         `scl_common.h <scl__common_8h.html#aa3b57f3b3f84f6414fbc49955b52323b>`__
      -  SCL_BADARG :
         `scl_common.h <scl__common_8h.html#ae6acee12ba2f2c10646820a5318f6bd6>`__
      -  SCL_BUFFER_ALLOC_FAIL :
         `scl_common.h <scl__common_8h.html#afe96ddd63a5e911ab9734e431b010db7>`__
      -  SCL_BUFFER_POINTER_MOVE_ERROR :
         `scl_common.h <scl__common_8h.html#aacd2dd6dadbf4220d362decd3ec4cc80>`__
      -  SCL_BUFFER_SIZE_SET_ERROR :
         `scl_common.h <scl__common_8h.html#a5882aa767bfe88aaad24512afa97a861>`__
      -  SCL_BUFFER_UNAVAILABLE_PERMANENT :
         `scl_common.h <scl__common_8h.html#abc10ed772792a39e89980135b9b831f9>`__
      -  SCL_BUFFER_UNAVAILABLE_TEMPORARY :
         `scl_common.h <scl__common_8h.html#ab9d5cb581d942c047b392dc7c965fd88>`__
      -  SCL_BUS_READ_REGISTER_ERROR :
         `scl_common.h <scl__common_8h.html#a82489c6172fc349133b0faf86776f2cb>`__
      -  SCL_BUS_WRITE_REGISTER_ERROR :
         `scl_common.h <scl__common_8h.html#a788952b9f0e276d8df9ee3de610b05b2>`__
      -  SCL_CLM_BLOB_DLOAD_ERROR :
         `scl_common.h <scl__common_8h.html#a7fc0f4f4394fc5467e87d3ba3305d484>`__
      -  SCL_CONNECTION_LOST :
         `scl_common.h <scl__common_8h.html#a147164e3c59553e9dd10006b2177cb7e>`__
      -  SCL_CORE_CLOCK_NOT_ENABLED :
         `scl_common.h <scl__common_8h.html#ae5012dfc795a129a3f64714525aa1e02>`__
      -  SCL_CORE_IN_RESET :
         `scl_common.h <scl__common_8h.html#a46d67d50f44ebbe2d2b516b58f8b20ae>`__
      -  SCL_DELAY_TOO_LONG :
         `scl_common.h <scl__common_8h.html#adf604df3c574c0ba6a87300b72a3ba30>`__
      -  SCL_DELAY_TOO_SHORT :
         `scl_common.h <scl__common_8h.html#ae13e62ab3ec077effdf3bc66ae626e74>`__
      -  SCL_DOES_NOT_EXIST :
         `scl_common.h <scl__common_8h.html#a27f8ccfef1cfdfd307d64a5325498938>`__
      -  SCL_EAPOL_KEY_FAILURE :
         `scl_common.h <scl__common_8h.html#a168b46abdc05efad43c0a11338a7bab2>`__
      -  SCL_EAPOL_KEY_PACKET_G1_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a34c3c4d9dbe5515f50529776d61fa2f0>`__
      -  SCL_EAPOL_KEY_PACKET_M1_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a4e9f540f4c64973328c2b56b1bdf0044>`__
      -  SCL_EAPOL_KEY_PACKET_M3_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a5484a20a77467db4f145b2eb8578c910>`__
      -  SCL_ERROR :
         `scl_common.h <scl__common_8h.html#afcef196f4c8d543377091c20a821882a>`__
      -  SCL_ETHERNET_SIZE :
         `scl_common.h <scl__common_8h.html#a59fd39f6b23dfcf261fc9413c919840d>`__
      -  SCL_EVENT_HANDLER_LIST_SIZE :
         `scl_types.h <scl__types_8h.html#af360cd56c533c5430e4a650933d5aca3>`__
      -  SCL_EVENT_NOT_REGISTERED :
         `scl_types.h <scl__types_8h.html#a0e18e36fa66280ef0b6df15a818234ea>`__
      -  SCL_FILTER_NOT_FOUND :
         `scl_common.h <scl__common_8h.html#a94ee0fc58fef7785b8f62e3c9178a448>`__
      -  SCL_FLOW_CONTROLLED :
         `scl_common.h <scl__common_8h.html#afc83c8f84b293410b50fc799ef3e50fc>`__
      -  SCL_HAL_ERROR :
         `scl_common.h <scl__common_8h.html#ab0a878abdbe284a38fdc85536016a07d>`__
      -  SCL_HANDLER_ALREADY_REGISTERED :
         `scl_common.h <scl__common_8h.html#a717fe3a815914d29e13080e72d44502c>`__
      -  SCL_HWTAG_MISMATCH :
         `scl_common.h <scl__common_8h.html#a8545cf3396f9472f70baec87b26ca975>`__
      -  SCL_INTERFACE_NOT_UP :
         `scl_common.h <scl__common_8h.html#a98026fc9756213cb3717749fb749f7cd>`__
      -  SCL_INVALID_DUTY_CYCLE :
         `scl_common.h <scl__common_8h.html#ad521e106005c683ab61d9615b0969dbb>`__
      -  SCL_INVALID_INTERFACE :
         `scl_common.h <scl__common_8h.html#ad6140ba3485a938ca3f65870468d9465>`__
      -  SCL_INVALID_JOIN_STATUS :
         `scl_common.h <scl__common_8h.html#a5897b37122798b4a186e9c7a4c1869d9>`__
      -  SCL_INVALID_KEY :
         `scl_common.h <scl__common_8h.html#a3b229cb9def5d820651c47911b64cfb8>`__
      -  SCL_IOCTL_FAIL :
         `scl_common.h <scl__common_8h.html#acb9d8da07a1ebb8b750405d20d44c0ca>`__
      -  SCL_JOIN_IN_PROGRESS :
         `scl_common.h <scl__common_8h.html#a3b49eceb733774d128be1a246f3c520e>`__
      -  SCL_LINK_HEADER :
         `scl_common.h <scl__common_8h.html#a86f46ec4ae9fbf1242eb43955b57922b>`__
      -  SCL_LINK_MTU :
         `scl_common.h <scl__common_8h.html#a61a890767f865802f5f7a035afda4c6c>`__
      -  SCL_LOG :
         `scl_common.h <scl__common_8h.html#afe0916a4a7aa61aec705c0eea7070ec3>`__
      -  SCL_LOG_ENABLE :
         `scl_common.h <scl__common_8h.html#a39c911a3ca34cd81bdd64bd96b948b6a>`__
      -  SCL_MALLOC_FAILURE :
         `scl_common.h <scl__common_8h.html#ae5ddfcf29e6075b791683f2edde7a33b>`__
      -  SCL_MAX_EVENT_SUBSCRIPTION :
         `scl_types.h <scl__types_8h.html#ae8ab4c5be1ff00ab7458f36cd1b67c71>`__
      -  SCL_NETWORK_NOT_FOUND :
         `scl_common.h <scl__common_8h.html#a3e135af2999d2a2912c87330b1c88681>`__
      -  SCL_NO_CREDITS :
         `scl_common.h <scl__common_8h.html#a41d7fa4a6b22c656ec08f2408c5b493a>`__
      -  SCL_NO_PACKET_TO_RECEIVE :
         `scl_common.h <scl__common_8h.html#a2339dba8b136ea2d0eb526892ef6822e>`__
      -  SCL_NO_PACKET_TO_SEND :
         `scl_common.h <scl__common_8h.html#a63b966b4f32b5a30b33ad391214fbe52>`__
      -  SCL_NOT_AUTHENTICATED :
         `scl_common.h <scl__common_8h.html#a6ad3b22c1e2869653dd5ecd612fc4673>`__
      -  SCL_NOT_KEYED :
         `scl_common.h <scl__common_8h.html#a13c2a01881f8198c433e37c000befc39>`__
      -  SCL_NULL_PTR_ARG :
         `scl_common.h <scl__common_8h.html#a2d325860a6c6f788f5c64894dd890839>`__
      -  SCL_OUT_OF_EVENT_HANDLER_SPACE :
         `scl_common.h <scl__common_8h.html#ae93602b58610afc8529b2178e3e2c5af>`__
      -  SCL_PARTIAL_RESULTS :
         `scl_common.h <scl__common_8h.html#a92ca5f463e1c2a83310e5df81491e0d4>`__
      -  SCL_PAYLOAD_MTU :
         `scl_common.h <scl__common_8h.html#aa4651e54d79668879d137dfda561c7af>`__
      -  SCL_PENDING :
         `scl_common.h <scl__common_8h.html#ae57e1328e4bd1d8ec34471b6088f7881>`__
      -  SCL_PHYSICAL_HEADER :
         `scl_common.h <scl__common_8h.html#a80e258e7a6efe1a41c52d9ca665e6bef>`__
      -  SCL_PMK_WRONG_LENGTH :
         `scl_common.h <scl__common_8h.html#aad172962be0f5647656e31f8203b69b4>`__
      -  SCL_QUEUE_ERROR :
         `scl_common.h <scl__common_8h.html#a700f846b3efab9ccbda3b2663c407603>`__
      -  SCL_RESULT_CREATE :
         `scl_common.h <scl__common_8h.html#ac2ae61090a6b87a10cba31d3a7ab7dbc>`__
      -  SCL_RESULT_TYPE :
         `scl_common.h <scl__common_8h.html#aa4b42e44356c70b6978ac8bfbba52d5e>`__
      -  SCL_RTOS_ERROR :
         `scl_common.h <scl__common_8h.html#a23d7912477b9d69ebcccd788dc0d72cf>`__
      -  SCL_RTOS_STATIC_MEM_LIMIT :
         `scl_common.h <scl__common_8h.html#ab2842cd2bb9d0d2f9b881a072f932298>`__
      -  SCL_RX_BUFFER_ALLOC_FAIL :
         `scl_common.h <scl__common_8h.html#a3afdd559df35bb55e420581884284587>`__
      -  SCL_SDIO_BUS_UP_FAIL :
         `scl_common.h <scl__common_8h.html#a592ef2a003492d6e82e3f7bca65ca296>`__
      -  SCL_SDIO_RETRIES_EXCEEDED :
         `scl_common.h <scl__common_8h.html#a961738b00ab6cc3fd0a895684b7d7bc3>`__
      -  SCL_SDIO_RX_FAIL :
         `scl_common.h <scl__common_8h.html#ac8be2d2070d1112b1ae8c6ee457dacb0>`__
      -  SCL_SEMAPHORE_ERROR :
         `scl_common.h <scl__common_8h.html#a77575fc99109a3953ef1e6c4f7c9be48>`__
      -  SCL_SET_BLOCK_ACK_WINDOW_FAIL :
         `scl_common.h <scl__common_8h.html#af4097c312e5ee146c0519ae2162b7b55>`__
      -  SCL_SLEEP_ERROR :
         `scl_common.h <scl__common_8h.html#a6c9a25e621bcf2c49b2eea0273382534>`__
      -  SCL_SPI_ID_READ_FAIL :
         `scl_common.h <scl__common_8h.html#a9b9f49b2803ff123f9a4b72a50295bf1>`__
      -  SCL_SPI_SIZE_MISMATCH :
         `scl_common.h <scl__common_8h.html#a9125da1d1d934636999907d5d709ce0e>`__
      -  SCL_SUCCESS :
         `scl_common.h <scl__common_8h.html#afea4b7a41665fefdc814c971d1b0e893>`__
      -  SCL_THREAD_CREATE_FAILED :
         `scl_common.h <scl__common_8h.html#ae68f734c5fbed881e2ec0c001867232a>`__
      -  SCL_THREAD_DELETE_FAIL :
         `scl_common.h <scl__common_8h.html#a4304ef6c5c45329d188cfe066bf02d17>`__
      -  SCL_THREAD_FINISH_FAIL :
         `scl_common.h <scl__common_8h.html#a266b3500ad62ffa18e1069f5f183bb8f>`__
      -  SCL_THREAD_STACK_NULL :
         `scl_common.h <scl__common_8h.html#a81ae876334eab64ba3f3c52b14eae291>`__
      -  SCL_TIMEOUT :
         `scl_common.h <scl__common_8h.html#a4708918902cf76addf76a04f59ba47e8>`__
      -  SCL_UNFINISHED :
         `scl_common.h <scl__common_8h.html#a622ce501adf697709e64a1d536af64b0>`__
      -  SCL_UNKNOWN_INTERFACE :
         `scl_common.h <scl__common_8h.html#a2eaf58b9ca04ab501de5de179343d064>`__
      -  SCL_UNKNOWN_SECURITY_TYPE :
         `scl_common.h <scl__common_8h.html#ad10521dcba2930b53b3ba874cca55651>`__
      -  SCL_UNSUPPORTED :
         `scl_common.h <scl__common_8h.html#a322e94949e22bcd0a7543b37d6f7db9e>`__
      -  SCL_WAIT_ABORTED :
         `scl_common.h <scl__common_8h.html#a90f5a1884be7a65b04cf1c8af0721ad0>`__
      -  SCL_WEP_KEYLEN_BAD :
         `scl_common.h <scl__common_8h.html#a228b544da9140f31627089f0ab5b7320>`__
      -  SCL_WEP_NOT_ALLOWED :
         `scl_common.h <scl__common_8h.html#a67920030a5d135b0caa6dca8cd71e495>`__
      -  SCL_WPA_KEYLEN_BAD :
         `scl_common.h <scl__common_8h.html#aa2fb9dcf5bb1ded56653f15651c09e3b>`__
      -  SDPCM_HEADER :
         `scl_common.h <scl__common_8h.html#aaf73ac170e07969ed067a6cd5088044f>`__
      -  SHARED_ENABLED :
         `scl_types.h <scl__types_8h.html#a4d4a4586c264fe8e4acb0bf7169b7b0f>`__
      -  SSID_NAME_SIZE :
         `scl_types.h <scl__types_8h.html#a9ee2fe056ad3787e30bb8da7248592c7>`__

      .. rubric:: - t -
         :name: t--

      -  TIMER_DEFAULT_VALUE :
         `scl_ipc.h <scl__ipc_8h.html#a6788e80e528103971ca7827491d4dd86>`__
      -  TKIP_ENABLED :
         `scl_types.h <scl__types_8h.html#a20f0d7118c2d35a688bcdc5b8b0920d9>`__

      .. rubric:: - u -
         :name: u--

      -  UNUSED_PARAMETER :
         `scl_types.h <scl__types_8h.html#a3c95a90e7806e4b0d21edfae15b73465>`__
      -  UNUSED_VARIABLE :
         `scl_types.h <scl__types_8h.html#a4048bf3892868ded8a28f8cbdd339c09>`__

      .. rubric:: - w -
         :name: w--

      -  WEP_ENABLED :
         `scl_types.h <scl__types_8h.html#a4199a1b37dba92f482ff0b9eb406c323>`__
      -  WIFI_ON_TIMEOUT :
         `scl_ipc.h <scl__ipc_8h.html#a9d7167828e79659a9416cfa97c39a415>`__
      -  WPA2_SECURITY :
         `scl_types.h <scl__types_8h.html#a8875737a0403d2136a69bbc96401cccf>`__
      -  WPA3_SECURITY :
         `scl_types.h <scl__types_8h.html#ad175824d1581f69f5a725e4c9171aa79>`__
      -  WPA_SECURITY :
         `scl_types.h <scl__types_8h.html#aa7ccd472bacbd8ee01f31f0f0e2ce3dc>`__
      -  WPS_ENABLED :
         `scl_types.h <scl__types_8h.html#aaae7e8a0eb357cae8f130f9099d8e7b8>`__



