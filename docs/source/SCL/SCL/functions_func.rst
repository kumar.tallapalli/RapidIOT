==========
Functions
==========

   .. container:: contents

       
      .. rubric:: - a -
         :name: a--

      -  add_multicast_group() :
         SCL_EMAC 

      .. rubric:: - c -
         :name: c--

      -  connect() :
         SclSTAInterface 

      .. rubric:: - d -
         :name: d--

      -  disconnect() :
         SclSTAInterface 

      .. rubric:: - g -
         :name: g--

      -  get_align_preference() :
         SCL_EMAC 
      -  get_bss_type() :
         SclAccessPoint 
      -  get_bssid() :
         SclSTAInterface 
      -  get_default_instance() :
         SclSTAInterface 
      -  get_hwaddr() :
         SCL_EMAC 
      -  get_hwaddr_size() :
         SCL_EMAC 
      -  get_ie_data() :
         SclAccessPoint 
      -  get_ie_len() :
         SclAccessPoint 
      -  get_ifname() :
         SCL_EMAC 
      -  get_instance() :
         SCL_EMAC 
      -  get_mtu_size() :
         SCL_EMAC 
      -  get_rssi() :
         SclSTAInterface 
		 
      .. rubric:: - i -
         :name: i--

      -  is_interface_connected() :
         SclSTAInterface 

      .. rubric:: - l -
         :name: l--

      -  link_out() :
         SCL_EMAC 

      .. rubric:: - o -
         :name: o--

      -  operator=() :
         SclAccessPoint 

      .. rubric:: - p -
         :name: p--

      -  power_down() :
         SCL_EMAC 
      -  power_up() :
         SCL_EMAC 

      .. rubric:: - r -
         :name: r--

      -  remove_multicast_group() :
         SCL_EMAC 

      .. rubric:: - s -
         :name: s--

      -  scan() :
         SclSTAInterface 
      -  set_activity_cb() :
         SCL_EMAC 
      -  set_all_multicast() :
         SCL_EMAC 
      -  set_blocking() :
         SclSTAInterface 
      -  set_channel() :
         SclSTAInterface 
      -  set_credentials() :
         SclSTAInterface
      -  set_hwaddr() :
         SCL_EMAC 
      -  set_link_input_cb() :
         SCL_EMAC 
      -  set_link_state_cb() :
         SCL_EMAC 
      -  set_memory_manager() :
         SCL_EMAC 

      .. rubric:: - w -
         :name: w--

      -  wifi_on() :
         SclSTAInterface 
      -  wifi_set_up() :
         SclSTAInterface 

