========
Modules
========


   .. container:: contents

      .. container:: textblock

         Here is a list of all modules:

      .. container:: directory

         +----------------------------------+----------------------------------+
         |  \ `SCL communication            | APIs for communicating with      |
         | AP                               | Network Processor and added      |
         | I <group__communication.html>`__ | communicating eg.                |
         +----------------------------------+----------------------------------+
         |  \ `SCL Wi-Fi                    | APIs for controlling the Wi-Fi   |
         | API <group__wifi.html>`__        | system                           |
         +----------------------------------+----------------------------------+
		 
		 

.. note::
   Creating sample note to verify the rst content.
   




.. toctree::
   :maxdepth: 8
   :hidden:
   
   group__communication.rst
   group__wifi.rst
   
   
   

