=============
Enumarations
=============

-  scl_802_11_band_t : `scl_types.h <scl__types_8h.html#ad051fe45f0dabb3458843ea84a66a64d>`__
-  scl_bool_t : `scl_common.h <scl__common_8h.html#ad0c0e3eeace86bed54540fcfb97c8812>`__
-  scl_bss_type_t : `scl_types.h <scl__types_8h.html#a2f3bd2bff982264490a05a98fe12a92f>`__
-  scl_buffer_dir_t : `scl_buffer_api.h <scl__buffer__api_8h.html#a170d11659d05b9fae1041771132a52a0>`__
-  scl_event_num_t : `scl_types.h <scl__types_8h.html#aef70e7ecee07897e718f9471559e6504>`__
-  scl_interface_role_t : `scl_common.h <scl__common_8h.html#a491a0d6079223bcc144618147f16b263>`__
-  scl_ipc_rx_t : `scl_common.h <scl__common_8h.html#ad2284e2664d908a015ded6f3b6c9e6f8>`__
-  scl_ipc_tx_t : `scl_common.h <scl__common_8h.html#af4838d96b6edc8577f92d03e9ae66d46>`__
-  scl_nsapi_connection_status_t : `scl_common.h <scl__common_8h.html#aafcfea88c2d16fddafb15745a6ecaba8>`__
-  scl_nsapi_security_t : `scl_common.h <scl__common_8h.html#a14cb286a8d0d1925109eca3afce127b3>`__
-  scl_scan_status_t : `scl_types.h <scl__types_8h.html#a3936863aacd5c7f6573aa494ef322a1b>`__
-  scl_scan_type_t : `scl_types.h <scl__types_8h.html#a6ac4a99c8190f2f61e63b7fc5349f47e>`__
-  scl_security_t : `scl_types.h <scl__types_8h.html#af9ee3597a0970d1df20492b765892a67>`__
